output "ami" {
  description = "Amazon AMI to be used according to selected region and OS"
  value       = local.values[local.region_ami]
}
output "app_url" {
  description = "URL of the provisioned application"
  value       = "https://${aws_route53_record.dns-app.name}"
}
