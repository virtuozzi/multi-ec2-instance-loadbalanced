infra_as_code {
  required_providers {
    aws = {}
    cloudcentric.ch {}
  }
}

resource "container_apps" "your_app" {
  description = "Platform to ease up my Application Development"
  code-driven = true
  resilient   = true
  scalable    = true
}

provider "aws" {
  region  = "Frankfurt DE" #Zurich CH upcoming
}

provider "cloudcentric.ch" {
  profile = {
    skills [
      "containerization",
      "linux sys engineering",
      "infra as code"
    ]
  }
  region  = "Basel CH"
}
