resource "aws_launch_configuration" "web" {
  name_prefix                 = "${var.project_name}-"
  image_id                    = local.values[local.region_ami]
  instance_type               = var.instance_type
  key_name                    = var.key_name
  security_groups             = [aws_security_group.inbound-ssh-and-web.id]
  associate_public_ip_address = true
  user_data                   = templatefile(var.userdata_file, local.vars)
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "web" {
  name                      = "${aws_launch_configuration.web.name}-asg"
  min_size                  = var.asg_min_size
  desired_capacity          = var.asg_desired_size
  max_size                  = var.asg_max_size
  target_group_arns         = [aws_lb_target_group.back_end_80.arn]
  health_check_type         = "ELB"
  health_check_grace_period = "300"
  wait_for_capacity_timeout = "5m"
  wait_for_elb_capacity     = var.asg_desired_size
  launch_configuration      = aws_launch_configuration.web.name
  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]
  metrics_granularity = "1Minute"
  vpc_zone_identifier = aws_subnet.public-subnet.*.id
  tags = concat(
    [
      {
        "key"                 = "Environment"
        "value"               = var.env
        "propagate_at_launch" = true
      },
      {
        "key"                 = "Project Name"
        "value"               = var.project_name
        "propagate_at_launch" = true
      },
    ],
  )
  lifecycle {
    create_before_destroy = true
  }

}

resource "aws_autoscaling_policy" "web_policy_up" {
  name                   = "web_policy_up"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.web.name
}

resource "aws_cloudwatch_metric_alarm" "web_cpu_alarm_up" {
  alarm_name          = "web_cpu_alarm_up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "60"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.web.name
  }

  alarm_description = "This metric monitor EC2 instance CPU utilization"
  alarm_actions     = [aws_autoscaling_policy.web_policy_up.arn]
}

resource "aws_autoscaling_policy" "web_policy_down" {
  name                   = "web_policy_down"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.web.name
}

resource "aws_cloudwatch_metric_alarm" "web_cpu_alarm_down" {
  alarm_name          = "web_cpu_alarm_down"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "240"
  statistic           = "Average"
  threshold           = "10"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.web.name
  }

  alarm_description = "This metric monitor EC2 instance CPU utilization"
  alarm_actions     = [aws_autoscaling_policy.web_policy_down.arn]
}
