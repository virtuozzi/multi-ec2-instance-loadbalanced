variable "aws_profile" {
  default = "default"
  type    = string
}
variable "project_name" {
  type    = string
  default = "project-space"
}
variable "env" {
  type    = string
  default = "dev"
}
variable "userdata_file" {
  type    = string
  default = "userdata.sh.tpl"
}
variable "key_name" {
  type    = string
  default = "./secrets/aws-generic"
}
variable "private_key_path" {
  type    = string
  default = "./secrets/aws-generic"
}
variable "public_key_path" {
  type    = string
  default = "./secrets/aws-generic.pub"
}
variable "instance_type" {
  type    = string
  default = "t2.micro"
}
variable "region" {
  type    = string
  default = "eu-central-1"
}
variable "os" {
  type    = string
  default = "amazon-linux"
  validation {
    condition     = can(regex("amazon-linux|ubuntu", var.os))
    error_message = "The OS has to be either \"amazon-linux\" or \"ubuntu\"."
  }
}
variable "cidr_block" {
  type    = string
  default = "10.73.0.0/16"
}
variable "cidr_incoming_lb" {
  type = list(any)
  default = [
    "0.0.0.0/0",
  ]
}
variable "cidr_incoming_instance" {
  type = list(any)
  default = [
    "0.0.0.0/0",
  ]
}
variable "domain_name" {
  type    = string
  default = "ssg-infra.ch"
}
variable "app_fqdn" {
  type    = string
  default = "myapp.ssg-infra.ch"
}
variable "sourcefiles" {
  type    = string
  default = "/var/www/html"
}
variable "userdata" {
  type = string
}
variable "gitplatform" {
  type    = string
  default = "gitlab.com"
}
variable "gituser" {
  type = string
}
variable "gitsource" {
  type = string
}
variable "gituserdata" {
  type = string
}
variable "gitdeploykey" {
  type    = string
  default = "./secrets/gitdeploykey"
}
variable "asg_min_size" {
  type    = number
  default = "2"
}
variable "asg_max_size" {
  type    = number
  default = "4"
}
variable "asg_desired_size" {
  type    = number
  default = "2"
}
