locals {
  region_ami = "${var.region}_${var.os}"
  values = {
    "us-east-1_amazon-linux"    = "ami-0be2609ba883822ec"
    "us-east-1_ubuntu"          = "ami-0885b1f6bd170450c"
    "eu-central-1_amazon-linux" = "ami-03c3a7e4263fd998c"
    "eu-central-1_ubuntu"       = "ami-0502e817a62226e03"
  }
}
locals {
  vars = {
    sourcefiles  = var.sourcefiles
    userdata     = var.userdata
    gitplatform  = var.gitplatform
    gituser      = var.gituser
    gitsource    = var.gitsource
    gituserdata  = var.gituserdata
    gitdeploykey = file(var.gitdeploykey)
  }
}
