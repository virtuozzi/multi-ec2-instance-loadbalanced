resource "aws_route53_record" "dns-app" {
  zone_id = data.aws_route53_zone.dns-zone.zone_id
  name    = var.app_fqdn
  type    = "CNAME"
  ttl     = 600
  records = [aws_lb.lb01.dns_name]
}

resource "aws_acm_certificate" "custom-cert" {
  domain_name       = var.app_fqdn
  validation_method = "DNS"
}

data "aws_route53_zone" "dns-zone" {
  name         = var.domain_name
  private_zone = false
}

resource "aws_route53_record" "dns-cert-validation" {
  for_each = {
    for dvo in aws_acm_certificate.custom-cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.dns-zone.zone_id
}

resource "aws_acm_certificate_validation" "cert-validation" {
  certificate_arn         = aws_acm_certificate.custom-cert.arn
  validation_record_fqdns = [for record in aws_route53_record.dns-cert-validation : record.fqdn]
}

resource "aws_lb" "lb01" {
  name            = "lb-${var.project_name}-${var.env}"
  subnets         = aws_subnet.public-subnet.*.id
  security_groups = [aws_security_group.inbound-web-to-lb.id]
  internal        = "false"
  idle_timeout    = "10"
  tags = {
    Name        = var.project_name
    Environment = var.env
  }
}

resource "aws_lb_listener" "front_end_443" {
  load_balancer_arn = aws_lb.lb01.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01" #"ELBSecurityPolicy-FS-1-2-Res-2020-10"
  certificate_arn   = aws_acm_certificate_validation.cert-validation.certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.back_end_80.arn
  }
}

resource "aws_lb_listener" "front_end_80" {
  load_balancer_arn = aws_lb.lb01.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_target_group" "back_end_80" {
  name     = "targetgroup-${var.project_name}-${var.env}"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.vpc.id
  stickiness {
    type    = "lb_cookie"
    enabled = "true"
  }
}
