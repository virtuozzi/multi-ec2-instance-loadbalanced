resource "aws_vpc" "vpc" {
  cidr_block           = var.cidr_block
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"
  instance_tenancy     = "default"

  tags = {
    Name = "vpc-${var.project_name}-${var.env}"
  }
}

resource "aws_subnet" "public-subnet" {
  count                   = length(data.aws_availability_zones.available.names)
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = cidrsubnet(var.cidr_block, 8, count.index)
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true

  tags = {
    "Name" = "Public subnet - ${element(data.aws_availability_zones.available.names, count.index)}"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "igw-${var.env}"
  }
}

resource "aws_route_table" "rt" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "cloud-route-table-${var.env}"
  }
}

resource "aws_route_table_association" "rta-public-subnet" {
  count          = length(data.aws_availability_zones.available.names)
  subnet_id      = aws_subnet.public-subnet[count.index].id
  route_table_id = aws_route_table.rt.id
}

resource "aws_network_acl" "default" {
  vpc_id = aws_vpc.vpc.id
}

resource "aws_network_acl_rule" "ssh" {
  network_acl_id = aws_network_acl.default.id
  rule_number    = 200
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = [var.cidr_incoming_instance]
  from_port      = 22
  to_port        = 22
}

resource "aws_network_acl_rule" "web-https" {
  network_acl_id = aws_network_acl.default.id
  rule_number    = 200
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = var.cidr_incoming_lb
  from_port      = 443
  to_port        = 443
}
resource "aws_network_acl_rule" "web-http" {
  network_acl_id = aws_network_acl.default.id
  rule_number    = 200
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = var.cidr_incoming_lb
  from_port      = 80
  to_port        = 80
}
