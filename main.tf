terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

resource "aws_key_pair" "aws-generic" {
  key_name   = var.key_name
  public_key = file(var.public_key_path)
}

provider "aws" {
  profile = var.aws_profile
  region  = var.region
}

data "aws_availability_zones" "available" {
  state = "available"
}
