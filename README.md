# Architecture
![Layout](media/multi-ec2-loadbalanced.png)
# Autoscaling Webserver-Deployment with Loadbalancer

- Highly available web-setup used as base for web-applications (apache, php and git available out-of-the-box)
- Autoscaling in place which automatically decreases/increases amount of instances according to cpu-load
- Min/Max/Desired size can be set with according variable
- Loadbalancer uses all available web instances as backends and uses session stickiness to prevent application malfunction
- FQDN and TLS-Certificate get's also automatically populated (owned domain at aws-layer is needed for this to function)

## Prerequisites

- Terraform >=0.14
- AWS access key and secret
- If own FQDN and TLS-Certificate is desirable for the application, a registered domain at AWS' Route53 Service is necessary

## Getting Started, general considerations

- Set up aws id and key @~/.aws/credentials
- Get terraform binary and make sure it's in your $PATH
- Clone this repo with `git clone`

### Customization of application contents

- create generic ssh keypair for administration of newly created resources and put it in secrets folder (./secrets/aws-generic for example)
- all application-specific items are processed through userdata.sh.tpl and are manageable with terraform variables (check "#APPLICATION" section)
- generate a second ssh key for git deployment/retrieval and place the private key also in secrets folder (./secrets/gitdeploykey for example)
- change the relevant variables at "terraform.tfvars"
- add the second generated public key for git deployment/retrieval to your git-platform as "deploy key" (userdata repo has to be writable for changes to be persistent, sourcefiles should remain read-only)
- no config mgmt like ansible is needed; setup is stateless and changes are done solely by terraform and its variables
- setup allows use of redhat-derivates like centos/rh and debian-derivates (amazon linux is also supported), os/ami selection happens automatically according to selected region

### Customization of terraform's provisioning environment

- Also make use of desired variables at "terraform.tfvars"
- Initialize terraform with `terraform init`
- Check the construction with `terraform plan` and build it with `terraform apply`
- Changes are done solely through "terraform.tfvars" and reapplying the status with `terraform apply`
- Force recreation of ressource(s) with terraform's "taint" subcommand if needed

# Authors

* **Michel Drozdz** - *Initial work* 

# License

This project is licensed under the GNU GPLv3 License.

# Acknowledgments

- Terraform Devs and Documentation
- Yevgeniy Brikman's "Terraform Up & Running"
