#!/bin/bash -xe
SOURCEFILES=${sourcefiles}
USERDATA=${userdata}
GITPLATFORM=${gitplatform}
GITUSER=${gituser}
GITSOURCE=${gitsource}
GITUSERDATA=${gituserdata}
GITDEPLOYKEY="${gitdeploykey}"

cd /tmp
if [ -e /usr/bin/yum ]; then
  yum update -y
  if [ -e /bin/amazon-linux-extras ]; then
    amazon-linux-extras install -y php7.2 epel
    yum install -y httpd git stress htop tmux
  else
    yum install -y httpd php git stress htop tmux
  fi
  install -d -g apache -o apache -m 0700 /usr/share/httpd/.ssh
  systemctl enable httpd --now
elif [ -e /usr/bin/apt ]; then
  apt update -y
  usermod -d /usr/share/httpd www-data
  apt install -y apache2 php php-sqlite3 git stress htop tmux
  systemctl enable apache2 --now
  install -d -g www-data -o www-data -m 0700 /usr/share/httpd/.ssh
else
  echo "Neither Debian, nor RedHat-derivate found, aborting..."
  exit 1
fi

echo "$GITDEPLOYKEY" > /usr/share/httpd/.ssh/gitdeploykey

chmod 600 /usr/share/httpd/.ssh/gitdeploykey

echo "Host $GITPLATFORM
   IdentityFile /usr/share/httpd/.ssh/gitdeploykey
   StrictHostKeyChecking no" > /usr/share/httpd/.ssh/config

if [ -e /usr/bin/yum ]; then
  chown -R apache. /usr/share/httpd/.ssh $SOURCEFILES
  chown apache. /usr/share/httpd/.ssh/config
  rm -rf $SOURCEFILES/{,.[!.],..?}*
  sudo -u apache git clone git@$GITPLATFORM:$GITUSER/$GITSOURCE.git $SOURCEFILES
  sudo -u apache git clone git@$GITPLATFORM:$GITUSER/$GITUSERDATA.git $USERDATA
elif [ -e /usr/bin/apt ]; then
  chown -R www-data. /usr/share/httpd/.ssh $SOURCEFILES
  chown www-data. /usr/share/httpd/.ssh/config
  rm -rf $SOURCEFILES/{,.[!.],..?}*
  sudo -u www-data git clone git@$GITPLATFORM:$GITUSER/$GITSOURCE.git $SOURCEFILES
  sudo -u www-data git clone git@$GITPLATFORM:$GITUSER/$GITUSERDATA.git $USERDATA
else
  echo "Neither Debian, nor RedHat-derivate found, aborting..."
  exit 1
fi
