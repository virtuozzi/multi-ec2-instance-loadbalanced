#MAIN SETTINGS
aws_profile  = "default"
region       = "eu-central-1"
project_name = "app"
env          = "dev"
#INSTANCE
os               = "amazon-linux"
instance_type    = "t2.micro"
asg_min_size     = "1"
asg_max_size     = "4"
asg_desired_size = "2"
key_name         = "./secrets/aws-generic"
userdata_file    = "userdata.sh.tpl"
public_key_path  = "./secrets/aws-generic.pub"
private_key_path = "./secrets/aws-generic"
#APPLICATION
sourcefiles  = "/var/www/html"
userdata     = "/var/www/html/userdata"
gitplatform  = "gitlab.com"
gituser      = "example"
gitsource    = "sources"
gituserdata  = "userdata"
gitdeploykey = "./secrets/gitdeploykey"
#NETWORK
cidr_block = "10.0.0.0/16"
cidr_incoming_lb = [
  "0.0.0.0/0",
]
cidr_incoming_instance = [
  "0.0.0.0/0",
]
domain_name = "example.com"
app_fqdn    = "app.example.com"
